package com.dyq;


import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BIOServer {
    public static void main(String[] args) throws IOException {

        //思路
        //1. 创建一个线程池
        //2. 如果有客户端连接，就创建一个线程，与之通讯(单独写一个方法)
        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();

        ServerSocket serverSocket = new ServerSocket(6666);
        System.out.println("服务器启动了");


        while (true) {

            System.out.println("线程信息 id =" + Thread.currentThread().getId() + " 名字=" + Thread.currentThread().getName());
            System.out.println("wating for connecting");

            //监听，等待客户端连接, 若无连接是阻塞状态
            final Socket accept = serverSocket.accept();
            System.out.println("connect to one client");
            newCachedThreadPool.execute(() -> {
                // 可以和客户端通讯
                handle(accept);
            });
        }
    }

    public static void handle(Socket socket) {
        System.out.println("线程信息 id =" + Thread.currentThread().getId() + " 名字=" + Thread.currentThread().getName());
        byte[] bytes = new byte[1024];
        try {
            //通过socket 获取输入流
            InputStream inputStream = socket.getInputStream();
            //循环的读取客户端发送的数据
            while (true) {
                System.out.println(Thread.currentThread().getName() + "---wating for reading");
                int read = inputStream.read(bytes);
                // -1代表读到文件尽头了
                if (read != -1) {
                    System.out.println(new String(bytes, 0, read));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("---closing connection");
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
