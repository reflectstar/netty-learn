package com.dyq.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class QueueTest {
    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue();
        queue.offer(5);
        queue.offer(1);
        queue.offer(8);
        FutureTask<List> futureTask = new FutureTask(() -> {
            Thread.sleep(3000);
            List list = new ArrayList<>();
            return list;
        });
        Thread thread = new Thread(futureTask);
        thread.start();
        try {
            List list = futureTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
