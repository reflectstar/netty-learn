package com.dyq.nio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Channel2File {
    public static void main(String[] args) {
        File file = new File("D:\\Java\\channel.txt");
        try {
            //创建一个输出流->channel
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            //通过 fileOutputStream 获取 对应的 FileChannel
            FileChannel channel = fileOutputStream.getChannel();
            String s = "真的会有人选择阿福吗";
            //创建一个缓冲区 ByteBuffer
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            //将 str 放入 byteBuffer
            byteBuffer.put(s.getBytes());
            //对byteBuffer 进行flip,准备进行写操作
            byteBuffer.flip();
            //将byteBuffer 数据写入到 fileChannel
            channel.write(byteBuffer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }
}
