package com.dyq.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class NIOClient {
    public static void main(String[] args) throws IOException {
        //得到一个网络通道
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        InetSocketAddress address = new InetSocketAddress("127.0.0.1", 6666);
        //连接服务器
        if (!socketChannel.connect(address)) {
            while (!socketChannel.finishConnect()) {
                System.out.println("client can do other thing...");
            }
        }
        String s = "hah shssssssssssssssssssssssssssssssssssssssssss";
        ByteBuffer byteBuffer = ByteBuffer.wrap(s.getBytes());
        socketChannel.write(byteBuffer);
        System.in.read();
    }
}
