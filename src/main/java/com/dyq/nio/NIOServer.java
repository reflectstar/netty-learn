package com.dyq.nio;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {
    public static void main(String[] args) throws IOException {
        //创建ServerSocketChannel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //通过channel得到ServerSocket，然后绑定（监听）一个端口
        serverSocketChannel.socket().bind(new InetSocketAddress(6666));
        //设置为非阻塞
        serverSocketChannel.configureBlocking(false);
        //得到一个Selecor对象
        Selector selector = Selector.open();
        //把 serverSocketChannel 注册到selector，设置关心事件为 OP_ACCEPT
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        while (true) {
            //select能返回各个通道处发生事件的数量
            if (selector.select(1000) == 0) {
                System.out.println("sys has been waiting for 1s");
                continue;
            }
            //如果返回的>0, 就获取到相关的 selectionKey集合
            //1.如果返回的>0， 表示已经获取到关注的事件
            //2. selector.selectedKeys() 返回关注事件的集合
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                //判断key 对应的通道发生的事件类型，做对应的处理
                if (key.isAcceptable()) {//代表OP_ACCEPT, 有新的客户端连接
                    //为该客户端的连接，新建一个socketChannel，相当于bio中的socket一般
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    socketChannel.configureBlocking(false);
                    //并将该通道注册到selector上，设置该通道的关注事件为OP_READ
                    socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(1024));
                }
                if (key.isReadable()) {//代表OP_READ，客户端有数据传送过来
                    //通过key 反向获取到对应channel
                    SocketChannel channel = (SocketChannel) key.channel();
                    //获取到该channel关联的buffer
                    ByteBuffer buffer = (ByteBuffer) key.attachment();
                    int read = channel.read(buffer);
                    System.out.println(new String(buffer.array()));
                }
                //手动从集合中移动当前的selectionKey, 防止重复操作
                iterator.remove();
            }
        }

    }
}
