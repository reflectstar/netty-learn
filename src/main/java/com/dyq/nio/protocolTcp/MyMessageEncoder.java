package com.dyq.nio.protocolTcp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class MyMessageEncoder extends MessageToByteEncoder<MessageProtocol> {
    @Override
    protected void encode(ChannelHandlerContext ctx, MessageProtocol msg, ByteBuf out) throws Exception {
        System.out.println("[MyMessageEncoder.encode]被调用");
        int len = msg.getLen();
        byte[] content = msg.getContent();
        out.writeInt(len);
        out.writeBytes(content);
    }
}
