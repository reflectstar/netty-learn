package com.dyq.nio.simple;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import io.netty.util.CharsetUtil;

import java.util.concurrent.TimeUnit;

public class NettyServerHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("[NettyServerHandler.channelRead]服务器读取线程 " + Thread.currentThread().getName() + " channle =" + ctx.channel());
        System.out.println("[NettyServerHandler.channelRead]server ctx =" + ctx);
        System.out.println("[NettyServerHandler.channelRead]看看channel 和 pipeline的关系");
        Channel channel = ctx.channel();
        ChannelPipeline pipeline = ctx.pipeline(); //本质是一个双向链接, 出站入站

        //将 msg 转成一个 ByteBuf
        //ByteBuf 是 Netty 提供的，不是 NIO 的 ByteBuffer.
        ByteBuf buf = (ByteBuf) msg;
        System.out.println("[NettyServerHandler.channelRead]客户端发送消息是:" + buf.toString(CharsetUtil.UTF_8));
        System.out.println("[NettyServerHandler.channelRead]客户端地址:" + channel.remoteAddress());


        //用户程序自定义的普通任务
        ctx.channel().eventLoop().execute(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5 * 1000);
                    ctx.writeAndFlush(Unpooled.copiedBuffer("hello, 客户端~(>^ω^<)喵2", CharsetUtil.UTF_8));
                    System.out.println("[NettyServerHandler.channelRead]channel code=" + ctx.channel().hashCode());
                } catch (Exception ex) {
                    System.out.println("[NettyServerHandler.channelRead]发生异常" + ex.getMessage());
                }
            }
        });

        // 因为nioloop只有一个线程，因此下面的代码要10s后才做完
        ctx.channel().eventLoop().execute(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5 * 1000);
                    ctx.writeAndFlush(Unpooled.copiedBuffer("hello, 客户端~(>^ω^<)喵3", CharsetUtil.UTF_8));
                    System.out.println("[NettyServerHandler.channelRead]channel code=" + ctx.channel().hashCode());
                } catch (Exception ex) {
                    System.out.println("[NettyServerHandler.channelRead]发生异常" + ex.getMessage());
                }
            }
        });

        // 用户自定义定时任务 -》 该任务是提交到 scheduleTaskQueue中
        ctx.channel().eventLoop().schedule(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(5 * 1000);
                    ctx.writeAndFlush(Unpooled.copiedBuffer("hello, 客户端~(>^ω^<)喵4", CharsetUtil.UTF_8));
                    System.out.println("[NettyServerHandler.channelRead]channel code=" + ctx.channel().hashCode());
                } catch (Exception ex) {
                    System.out.println("[NettyServerHandler.channelRead]发生异常" + ex.getMessage());
                }
            }
        }, 5, TimeUnit.SECONDS);


    }

    //数据读取完毕
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {

        //writeAndFlush 是 write + flush
        //将数据写入到缓存，并刷新
        //一般讲，我们对这个发送的数据进行编码
        ctx.writeAndFlush(Unpooled.copiedBuffer("[NettyServerHandler.channelReadComplete]hello, 客户端~(>^ω^<)喵1", CharsetUtil.UTF_8));
    }

    //处理异常, 一般是需要关闭通道

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        System.out.println("[NettyServerHandler.exceptionCaught]已关闭通道" + ctx.channel());
    }
}
