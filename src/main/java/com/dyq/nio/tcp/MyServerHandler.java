package com.dyq.nio.tcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.nio.charset.Charset;
import java.util.UUID;

public class MyServerHandler extends SimpleChannelInboundHandler<ByteBuf> {
    private int count;

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        int count = msg.readableBytes();
        byte[] bytes = new byte[count];
        msg.readBytes(bytes);
        String string = new String(bytes, Charset.forName("utf-8"));
        System.out.println("[MyServerHandler.channelRead0]服务端接受到的msg为：" + string);
        System.out.println("[MyServerHandler.channelRead0]接受到的数量为：" + ++this.count);
    }
}
